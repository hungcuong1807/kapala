
from __future__ import absolute_import, division, print_function, unicode_literals

import os.path
from io import StringIO
from os import path

import numpy as np
import torch
import pandas as pd
df_train = pd.read_csv("data.csv", encoding="utf-8")
# df_test = pd.read_csv("test.csv", encoding="utf-8")
cat_features = ['province', 'district', 'maCv',
                'FIELD_8', 'FIELD_9', 'FIELD_1', 'FIELD_2', 'FIELD_5',
                'FIELD_10', 'FIELD_13', 'FIELD_17',
                'FIELD_24', 'FIELD_35', 'FIELD_39', 'FIELD_12',
                'FIELD_41', 'FIELD_42', 'FIELD_43',
                'FIELD_44', 'FIELD_16', 'FIELD_21', 'FIELD_32', 'FIELD_33', 'FIELD_34', 'FIELD_40', 'FIELD_45', 'FIELD_46']
bool_features = ['FIELD_18', 'FIELD_19',
                 'FIELD_20', 'FIELD_23', 'FIELD_25',
                 'FIELD_26', 'FIELD_27', 'FIELD_28',
                 'FIELD_29', 'FIELD_30', 'FIELD_31',
                 'FIELD_36', 'FIELD_37', 'FIELD_38',
                 'FIELD_47', 'FIELD_48', 'FIELD_49']
new_cols = ['FIELD_7_AT',
            'FIELD_7_BT',
            'FIELD_7_PV',
            'FIELD_7_TL',
            'FIELD_7_CK',
            'FIELD_7_MS',
            'FIELD_7_TA',
            'FIELD_7_XD',
            'FIELD_7_HN',
            'FIELD_7_TN',
            'FIELD_7_TQ',
            'FIELD_7_CB',
            'FIELD_7_HG',
            'FIELD_7_TC',
            'FIELD_7_CC',
            'FIELD_7_HX',
            'FIELD_7_LS',
            'FIELD_7_XK',
            'FIELD_7_KC',
            'FIELD_7_QT',
            'FIELD_7_DT',
            'FIELD_7_TE',
            'FIELD_7_CH',
            'FIELD_7_XN',
            'FIELD_7_NO',
            'FIELD_7_TK',
            'FIELD_7_DN',
            'FIELD_7_GD',
            'FIELD_7_TS',
            'FIELD_7_HK',
            'FIELD_7_HS',
            'FIELD_7_ND',
            'FIELD_7_HT',
            'FIELD_7_CN',
            'FIELD_7_HC',
            'FIELD_7_SV',
            'FIELD_7_GB',
            'FIELD_7_XB',
            'FIELD_7_NN',
            'FIELD_7_QN',
            'FIELD_7_HD',
            'FIELD_7_TB',
            'FIELD_7_XV',
            'FIELD_7_DK']
num_features = [col for col in df_train.columns if col not in cat_features +
                bool_features + new_cols if col not in ['id', 'label']]


def convertUStringToDistinctInts(mat, convertDicts, counts):
        # Converts matrix of unicode strings into distinct integers.
        #
        # Inputs:
        #     mat (np.array): array of unicode strings to convert
        #     convertDicts (list): dictionary for each column
        #     counts (list): number of different categories in each column
        #
        # Outputs:
        #     out (np.array): array of output integers
        #     convertDicts (list): dictionary for each column
        #     counts (list): number of different categories in each column

        # check if convertDicts and counts match correct length of mat
    if len(convertDicts) != mat.shape[1] or len(counts) != mat.shape[1]:
        print("Length of convertDicts or counts does not match input shape")
        print("Generating convertDicts and counts...")

        convertDicts = [{} for _ in range(mat.shape[1])]
        counts = [0 for _ in range(mat.shape[1])]

    # initialize output
    out = torch.zeros(mat.shape)

    for j in range(mat.shape[1]):
        for i in range(mat.shape[0]):
            # add to convertDict and increment count
            if mat[i, j] not in convertDicts[j]:
                convertDicts[j][mat[i, j]] = counts[j]
                counts[j] += 1
            out[i, j] = convertDicts[j][mat[i, j]]

    return out, convertDicts, counts


def processKaggleCriteoAdData(split, d_path):
    # Process Kaggle Display Advertising Challenge Dataset by converting unicode strings
    # in X_cat to integers and converting negative integer values in X_int.
    #
    # Loads data in the form "kaggle_day_i.npz" where i is the day.
    #
    # Inputs:
    #   split (int): total number of splits in the dataset (typically 7)
    #   d_path (str): path for kaggle_day_i.npz files

    convertDicts = []
    counts = []

    # check if processed file already exists
    idx = 1
    while idx <= split:
        if path.exists(str(d_path) + "kaggle_day_{0}_processed.npz".format(idx)):
            idx += 1
        else:
            break

    # process data if not all files exist
    if idx <= split:
        for i in range(1, split + 1):
            with np.load(str(d_path) + "kaggle_day_{0}.npz".format(i)) as data:

                X_cat, convertDicts, counts = convertUStringToDistinctInts(
                    data["X_cat"], convertDicts, counts
                )
                X_int = data["X_int"]
                X_int[X_int < 0] = 0
                y = data["y"]
                ids = data["id"]
                print(ids)

            np.savez_compressed(
                str(d_path) + "kaggle_day_{0}_processed.npz".format(i),
                X_cat=X_cat,
                X_int=X_int,
                y=y,
                id=ids
            )
            print("Processed kaggle_day_{0}.npz...".format(i), end="\r")
        np.savez_compressed(str(d_path) + "kaggle_counts.npz", counts=counts)
    else:
        print("Using existing %skaggle_day_*_processed.npz files" % str(d_path))

    return


def concatKaggleCriteoAdData(split, d_path, o_filename):
    # Concatenates different days of Kaggle data and saves.
    #
    # Inputs:
    #   split (int): total number of splits in the dataset (typically 7)
    #   d_path (str): path for kaggle_day_i.npz files
    #   o_filename (str): output file name
    #
    # Output:
    #   o_file (str): output file path

    print("Concatenating multiple day kaggle data into %s.npz file" %
          str(d_path + o_filename))

    # load and concatenate data
    for i in range(1, split + 1):
        with np.load(str(d_path) + "kaggle_day_{0}_processed.npz".format(i)) as data:

            if i == 1:
                X_cat = data["X_cat"]
                X_int = data["X_int"]
                y = data["y"]
                ids = data["id"]
            else:
                X_cat = np.concatenate((X_cat, data["X_cat"]))
                X_int = np.concatenate((X_int, data["X_int"]))
                y = np.concatenate((y, data["y"]))
                ids = np.concatenate((ids, data["id"]))

        print("Loaded day:", i, "y = 1:", len(
            y[y == 1]), "y = 0:", len(y[y == 0]))

    with np.load(str(d_path) + "kaggle_counts.npz") as data:

        counts = data["counts"]

    print("Loaded counts!")

    np.savez_compressed(
        str(d_path) + str(o_filename) + ".npz",
        X_cat=X_cat,
        X_int=X_int,
        y=y,
        id=ids,
        counts=counts,
    )

    return str(d_path) + str(o_filename) + ".npz"


def transformCriteoAdData(X_cat, X_int, y, ids, split, randomize, cuda):
    # Transforms Kaggle data by applying log transformation on dense features and
    # converting everything to appropriate tensors.
    #
    # Inputs:
    #     X_cat (ndarray): array of integers corresponding to preprocessed
    #                      categorical features
    #     X_int (ndarray): array of integers corresponding to dense features
    #     y (ndarray): array of bool corresponding to labels
    #     split (bool): flag for splitting dataset into training/validation/test
    #                     sets
    #     randomize (str): determines randomization scheme
    #         "none": no randomization
    #         "day": randomizes each day"s data (only works if split = True)
    #         "total": randomizes total dataset
    #     cuda (bool): flag for enabling CUDA and transferring data to GPU
    #
    # Outputs:
    #     if split:
    #         X_cat_train (tensor): sparse features for training set
    #         X_int_train (tensor): dense features for training set
    #         y_train (tensor): labels for training set
    #         X_cat_val (tensor): sparse features for validation set
    #         X_int_val (tensor): dense features for validation set
    #         y_val (tensor): labels for validation set
    #         X_cat_test (tensor): sparse features for test set
    #         X_int_test (tensor): dense features for test set
    #         y_test (tensor): labels for test set
    #     else:
    #         X_cat (tensor): sparse features
    #         X_int (tensor): dense features
    #         y (tensor): label

    # define initial set of indices

    indices = np.arange(len(y))

    # split dataset
    if split:
        indices = indices[:30000]

        indices = np.array_split(indices, 5)

        # randomize each day"s dataset
        if randomize == "day" or randomize == "total":
            for i in range(len(indices)):
                indices[i] = np.random.permutation(indices[i])

        train_indices = np.concatenate(indices[:-1])
        test_indices = indices[-1]
        # val_indices, test_indices = np.array_split(test_indices, 2)

        print("Defined training and testing indices...")

        # randomize all data in training set
        if randomize == "total":
            train_indices = np.random.permutation(train_indices)
            print("Randomized indices...")

        # create training, validation, and test sets
        X_cat_train = X_cat[train_indices]
        X_int_train = X_int[train_indices]
        y_train = y[train_indices]
        id_train = ids[train_indices]

        # X_cat_val = X_cat[val_indices]
        # X_int_val = X_int[val_indices]
        # y_val = y[val_indices]
        # id_val = ids[val_indices]

        X_cat_test = X_cat[test_indices]
        X_int_test = X_int[test_indices]
        y_test = y[test_indices]
        id_test = ids[test_indices]

        print("Split data according to indices...")

        # convert to tensors
        if cuda:
            X_cat_train = torch.tensor(
                X_cat_train, dtype=torch.long).pin_memory()
            X_int_train = torch.log(
                torch.tensor(X_int_train, dtype=torch.float) + 1
            ).pin_memory()
            y_train = torch.tensor(y_train.astype(np.float32)).pin_memory()
            id_train = torch.tensor(id_train.astype(np.float32)).pin_memory()

            # X_cat_val = torch.tensor(X_cat_val, dtype=torch.long).pin_memory()
            # X_int_val = torch.log(
            #     torch.tensor(X_int_val, dtype=torch.float) + 1
            # ).pin_memory()
            # y_val = torch.tensor(y_val.astype(np.float32)).pin_memory()
            # id_val = torch.tensor(id_val.astype(np.float32)).pin_memory()

            X_cat_test = torch.tensor(
                X_cat_test, dtype=torch.long).pin_memory()
            X_int_test = torch.log(
                torch.tensor(X_int_test, dtype=torch.float) + 1
            ).pin_memory()
            y_test = torch.tensor(y_test.astype(np.float32)).pin_memory()

            id_test = torch.tensor(id_test.astype(np.float32)).pin_memory()
        else:
            X_cat_train = torch.tensor(X_cat_train, dtype=torch.long)
            X_int_train = torch.log(torch.tensor(
                X_int_train, dtype=torch.float) + 1)
            y_train = torch.tensor(y_train.astype(np.float32))
            id_train = torch.tensor(id_train.astype(np.float32))

            # X_cat_val = torch.tensor(X_cat_val, dtype=torch.long)
            # X_int_val = torch.log(torch.tensor(
            #     X_int_val, dtype=torch.float) + 1)
            # y_val = torch.tensor(y_val.astype(np.float32))
            # id_val = torch.tensor(id_val.astype(np.float32))

            X_cat_test = torch.tensor(X_cat_test, dtype=torch.long)
            X_int_test = torch.log(torch.tensor(
                X_int_test, dtype=torch.float) + 1)
            y_test = torch.tensor(y_test.astype(np.float32))
            id_test = torch.tensor(id_test.astype(np.float32))

        print("Converted to tensors...done!")

        return (
            X_cat_train,
            X_int_train,
            y_train,
            X_cat_test,
            X_int_test,
            y_test,
            id_train,
            id_test
        )

    else:
        indices = indices[30000:]

        # randomize data
        X_cat_test = torch.tensor(X_cat[indices], dtype=torch.long)
        X_int_test = torch.log(torch.tensor(
            X_int[indices], dtype=torch.float) + 1)
        y_test = torch.tensor(y[indices].astype(np.float32))
        ids = torch.tensor(ids[indices].astype(np.float32))
        print("Converted to tensors...done!")

        return X_cat_test, X_int_test, y_test, ids


def convert_row(df, index):

    ints = []
    cats = []
    label = df.at[index, "label"]
    if pd.isna(df.at[index, "label"]):
        label = 0
    ids = df.at[index, "id"]
    for col in num_features:
        ints.append(df.at[index, col])
    for col in (cat_features+bool_features+new_cols):
        cats.append(df.at[index, col])
    return {'label': label, 'int_feature': ints, 'cat_feature': cats, 'id': ids}


def getKaggleCriteoAdData(o_filename="", type_data="train"):
    # Passes through entire dataset and defines dictionaries for categorical
    # features and determines the number of total categories.
    #
    # Inputs:
    #    datafile : path to downloaded raw data file
    #    o_filename (str): saves results under o_filename if filename is not ""
    #
    # Output:
    #   o_file (str): output file path

    d_path = "data/"
    df = df_train
    # determine if intermediate data path exists
    if path.isdir(str(d_path)):
        print("Saving intermediate data files at %s" % (d_path))
    else:
        os.mkdir(str(d_path))
        print("Created %s for storing intermediate data files" % (d_path))

    total_count = len(list(df.index))

    print("Total number of datapoints:", total_count)

    # determine length of split over 7 days
    split = 1
    num_data_per_split, extras = divmod(total_count, 1)

    # initialize data to store
    len_int = len(num_features)
    len_cat = len(
        cat_features+bool_features+new_cols)
    y = np.zeros(total_count, dtype="i4")
    id_row = np.zeros(total_count, dtype="i4")
    X_int = np.zeros((total_count, len_int), dtype="f4")
    X_cat = np.zeros((total_count, len_cat), dtype="U8")

    # check if files exist
    count = 0
    for i in df.index:
        data = convert_row(df, i)
        y[i - count] = data["label"]
        id_row[i-count] = data["id"]
        X_int[i - count] = data["int_feature"]
        X_cat[i - count] = data["cat_feature"]
        print(
            "Loading %d/%d   Split: %d   No Data in Split: %d  true label: %d  stored label: %d"
            % (
                i,
                total_count,
                split,
                num_data_per_split,
                data["label"],
                y[i - count],
            ),
            end="\r",
        )
    np.savez_compressed(
        str(d_path) + "kaggle_day_{0}.npz".format(split),
        X_int=X_int,
        X_cat=X_cat,
        y=y,
        id=id_row
    )

    processKaggleCriteoAdData(split, d_path)
    o_file = concatKaggleCriteoAdData(split, d_path, o_filename)

    return o_file


def loadDataset(dataset, num_samples, data=""):
    if dataset == "kaggle":
        df_exists = path.exists(str(data))
        if df_exists:
            print("Reading from pre-processed data=%s" % (str(data)))
            file = str(data)
        else:
            o_filename = "kaggleAdDisplayChallenge_processed"
            file = getKaggleCriteoAdData(o_filename)
    elif dataset == "terabyte":
        file = "./terbyte_data/tb_processed.npz"
        df_exists = path.exists(str(file))
        if df_exists:
            print("Reading Terabyte data-set processed data from %s" % file)
        else:
            raise (
                ValueError(
                    "Terabyte data-set processed data file %s does not exist !!" % file
                )
            )

    # load and preprocess data
    with np.load(file) as data:

        X_int = data["X_int"]
        X_cat = data["X_cat"]
        y = data["y"]
        counts = data["counts"]
        id = data["id"]

    return X_cat, X_int, y, counts, id


if __name__ == "__main__":
    getKaggleCriteoAdData(
        o_filename="kaggle_processed", type_data="train")
