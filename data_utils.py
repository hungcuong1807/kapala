import pandas as pd
import numpy as np
import math
import ast
from category_encoders import *
from sklearn.preprocessing import LabelEncoder
df_train = df = pd.read_csv('data/train.csv', encoding='utf-8')
df_test = pd.read_csv('data/test.csv', encoding='utf-8')

cat_features = ['province', 'district', 'maCv',
                'FIELD_7', 'FIELD_8', 'FIELD_9', 'FIELD_1', 'FIELD_2', 'FIELD_5',
                'FIELD_10', 'FIELD_13', 'FIELD_17',
                'FIELD_24', 'FIELD_35', 'FIELD_39', 'FIELD_12',
                'FIELD_41', 'FIELD_42', 'FIELD_43',
                'FIELD_44', 'FIELD_16', 'FIELD_21', 'FIELD_32', 'FIELD_33', 'FIELD_34', 'FIELD_40', 'FIELD_45', 'FIELD_46']
bool_features = ['FIELD_18', 'FIELD_19',
                 'FIELD_20', 'FIELD_23', 'FIELD_25',
                 'FIELD_26', 'FIELD_27', 'FIELD_28',
                 'FIELD_29', 'FIELD_30', 'FIELD_31',
                 'FIELD_36', 'FIELD_37', 'FIELD_38',
                 'FIELD_47', 'FIELD_48', 'FIELD_49']
num_features = [col for col in df_train.columns if col not in cat_features +
                bool_features if col not in ['id', 'label']]

text_cols = ['province', 'district', 'maCv', 'FIELD_8', 'FIELD_9', 'FIELD_10', 'FIELD_12',
             'FIELD_13', 'FIELD_17', 'FIELD_24', 'FIELD_39', 'FIELD_40', 'FIELD_41', 'FIELD_43']
not_text_cols = [col for col in cat_features if col not in text_cols]


def cleanNanTextColumns(df):
    df_m = df
    for col in text_cols:
        df_m[col] = df_m[col].fillna("not_inserted")
    return df_m


def cleanNanCatNotText(df):
    df_m = df
    for col in not_text_cols:
        if col != "FIELD_7":
            df_m[col] = df_m[col].fillna(-999)
            df_m[col] = df_m[col].replace("None", -1)
    return df_m


def clean_boolean_value(df):
    df_m = df
    bool_feats = [
        col for col in bool_features if col in np.asarray(df.columns)]
    print(bool_feats)
    for col in bool_feats:
        df_m.loc[df[col] == True, col] = 1
        df_m.loc[df[col] == False, col] = 0
        df_m[col] = df_m[col].fillna(-999)
    return df_m


def impute_numerical(df, normalize=False):
    num_feat = [col for col in num_features if col in np.asarray(
        df.columns) if col != 'label']
    print(num_feat)
    df_m = df
    for col in num_feat:
        col_median = df[col].median(skipna=True)
        df_m[col].fillna(col_median, inplace=True)
        if normalize and col != 'age_source2':
            df_m[col] = pd.to_numeric(df_m[col])
            df_m[col + "_normalize"] = (df_m[col] - np.min(df_m[col])) / \
                (np.max(df_m[col]) - np.min(df_m[col]))
    return df_m


def impute_age(df):
    df_m = df
    for index, row in df_m.iterrows():
        if row['age_source2'] != row['age_source1']:
            if math.isnan(row['age_source2']):
                row['age_source2'] = row['age_source1']
    return df_m


def missing_df(df):
    """
    return a data frame containing the statistics of missing value
    """

    total = df.isnull().sum().sort_values(ascending=False)
    fraction = 100 * total / df.shape[0]

    # keep to two decimal places
    fraction = fraction.apply(lambda x: round(x, 2))

    df_missing = pd.concat([total, fraction], axis=1,
                           keys=['Total', 'Fraction'])
    df_missing.index.name = 'Attributes'

    return df_missing


def cut_missing_fea(df, mis_threshold=70):
    missing = missing_df(df)
    keep_fea = missing[missing['Fraction'] < mis_threshold].index

    return df[keep_fea]


def groupMacv(df):
    df_m = df
    for i in df_m.index:
        if not pd.isna(df_m.at[i, "maCv"]):
            s = df_m.at[i, "maCv"].lower()
            # print(s)
            if "công nhân" in s or "cn" in s:
                df_m.at[i, "maCv"] = "công nhân"
            elif "nhân viên" in s or "nv" in s:
                df_m.at[i, "maCv"] = "nhân viên"
            elif "giáo viên" in s:
                df_m.at[i, "maCv"] = "giáo viên"
            elif "lái xe" in s or "Lỏi xe" in s or "tài xế" in s:
                df_m.at[i, "maCv"] = "lái xe"
            elif "kĩ sư" in s or "kỹ sư" in s:
                df_m.at[i, "maCv"] = "kĩ sư"
    return df_m


def handleMultiCat(df, normalize=False):
    df_transform = df
    # cat_col = [col for col in cat_features +
    #            bool_features if col in np.asarray(df.columns)]

    # count-encoding
    # cat_col_counts = [
    #     col for col in cat_col if col in np.asarray(df_transform.columns)]
    # for col in cat_col_counts:
    #     new_col = 'count_'+col
    #     df_transform[new_col] = df[col].map(df[col].value_counts())
    #     if normalize:
    #         df_transform[new_col] = (df_transform[new_col] - np.min(df_transform[new_col])) / (
    #             np.max(df_transform[new_col]) - np.min(df_transform[new_col]))

    df_transform['FIELD_7'] = df_transform['FIELD_7'].fillna("[]")

    field_7_value = set([])
    for t in np.asarray(df['FIELD_7'].unique()):
        if not pd.isna(t):
            field_7_value.update(set(ast.literal_eval(t)))
    field_7_value = list(field_7_value)

    for ind in df_transform.index:

        if not pd.isna(df_transform.at[ind, 'FIELD_7']):
            list_value = ast.literal_eval(df_transform.at[ind, 'FIELD_7'])
            for value in field_7_value:
                if value in list_value:
                    df_transform.at[ind, 'FIELD_7_'+value] = 1
                else:
                    df_transform.at[ind, 'FIELD_7_'+value] = 0
    df_transform = df_transform.drop(columns=['FIELD_7'])
    return df_transform


def convertStringCat(df):
    df_m = df
    new_cols = ['FIELD_7_AT',
                'FIELD_7_BT',
                'FIELD_7_PV',
                'FIELD_7_TL',
                'FIELD_7_CK',
                'FIELD_7_MS',
                'FIELD_7_TA',
                'FIELD_7_XD',
                'FIELD_7_HN',
                'FIELD_7_TN',
                'FIELD_7_TQ',
                'FIELD_7_CB',
                'FIELD_7_HG',
                'FIELD_7_TC',
                'FIELD_7_CC',
                'FIELD_7_HX',
                'FIELD_7_LS',
                'FIELD_7_XK',
                'FIELD_7_KC',
                'FIELD_7_QT',
                'FIELD_7_DT',
                'FIELD_7_TE',
                'FIELD_7_CH',
                'FIELD_7_XN',
                'FIELD_7_NO',
                'FIELD_7_TK',
                'FIELD_7_DN',
                'FIELD_7_GD',
                'FIELD_7_TS',
                'FIELD_7_HK',
                'FIELD_7_HS',
                'FIELD_7_ND',
                'FIELD_7_HT',
                'FIELD_7_CN',
                'FIELD_7_HC',
                'FIELD_7_SV',
                'FIELD_7_GB',
                'FIELD_7_XB',
                'FIELD_7_NN',
                'FIELD_7_QN',
                'FIELD_7_HD',
                'FIELD_7_TB',
                'FIELD_7_XV',
                'FIELD_7_DK']
    cat_cols = [col for col in cat_features +
                bool_features + new_cols if col in df.columns]
    for col in cat_cols:
        df_m[col] = df_m[col].astype(str)
    return df_m


cleanup_nums = {
    "FIELD_17": {'None': np.nan},
    "FIELD_24": {'None': np.nan, 'K3': 3, 'K1': 1, 'K2': 2},
    "FIELD_29": {'None': np.nan, 'FALSE': False, 'TRUE': True},
    "FIELD_30": {'None': np.nan, 'FALSE': False, 'TRUE': True},
    "FIELD_31": {'None': np.nan, 'FALSE': False},
    "FIELD_35": {'Zero': 0, 'One': 1, 'Two': 2, 'Three': 3, 'Four': 4},
    "FIELD_36": {'None': np.nan},
    "FIELD_37": {'TRUE': True, 'FALSE': False, 'None': np.nan},
    "FIELD_41": {'I': 1, 'II': 2, 'III': 3, 'IV': 4, 'V': 5, 'None': np.nan},
    "FIELD_43": {'0': 0, 'A': 1, 'B': 2, 'C': 3, 'D': 4, '5': 5, 'None': np.nan},
    "FIELD_44": {'None': np.nan, 'One': 1, 'Two': 2},
    "FIELD_45": {'1': 1.0, '2': 2.0, 'None': np.nan},
    "FIELD_9": {'na': np.nan, '75': np.nan, '80': np.nan, '86': np.nan, '79': np.nan},
    "maCv": {'None': np.nan},
    "FIELD_11": {'None': np.nan},
    "FIELD_12": {'None': np.nan, '0': np.nan, '1': np.nan},
    "FIELD_39": {'None': np.nan},
    "FIELD_40": {'None': np.nan, '1': 1, '2': 2, '02 05 08 11': -1, '08 02': -1, '05 08 11 02': -1, '3': 3, '6': 6, '4': 4},
    "FIELD_42": {'Zezo': 0, 'One': 1, 'Two': 2, 'None': np.nan},
    "FIELD_36": {'FALSE': False, 'TRUE': True, 'None': np.nan},
    "FIELD_10": {'None': np.nan}
}

if __name__ == "__main__":
    df_train.replace(cleanup_nums, inplace=True)
    df_test.replace(cleanup_nums, inplace=True)
    # df_train = cut_missing_fea(df_train)
    # df_test = cut_missing_fea(df_test)

    concat_df = pd.concat([df_train, df_test], ignore_index=True)
    concat_df = impute_age(concat_df)
    concat_df = concat_df.drop(columns=['age_source1'])

    concat_df = clean_boolean_value(concat_df)

    concat_df = cleanNanTextColumns(concat_df)
    concat_df = cleanNanCatNotText(concat_df)

    concat_df = impute_numerical(concat_df, False)
    concat_df = groupMacv(concat_df)

    concat_df = handleMultiCat(concat_df)
    concat_df = convertStringCat(concat_df)
    concat_df.to_csv("data.csv")

    train_df = concat_df[concat_df['label'].notnull()]
    test_df = concat_df[concat_df['label'].isnull()]

    train_df.to_csv("train.csv")
    test_df.to_csv("test.csv")
